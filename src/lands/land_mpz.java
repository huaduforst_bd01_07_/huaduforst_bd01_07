package lands;
import java.util.*;

import trees.tree_mpz;
import trees.tree_mzc;
//import trees.tree_mzc;
//import trees.tree_zgh;
import trees.tree_zgh;

public class land_mpz {
	public static int last;
	public String landID;
	public tree_mpz treeSet[];
	public land_mpz()
	{
		landID="MoPanzhong_land";
		treeSet=new tree_mpz[50];
		last=0;
	}
	public void plant(tree_mpz tree)
	{
		treeSet[last]=tree;
		last++;
	}
	public void water()
	{
		for(int i=0;i<last;i++)
		{
			treeSet[i].water();
		}
	}

}
