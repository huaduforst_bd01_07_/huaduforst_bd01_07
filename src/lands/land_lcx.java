package lands;

import trees.tree_lcx;
public class land_lcx {


	public String landID;
	public tree_lcx treeSet[];
	public static int last;
	public land_lcx() {
		landID="lcx_land";
		treeSet=new tree_lcx[50];
		last=0;
	}
	
	public void plant(tree_lcx tree){
		treeSet[last]=tree;
		last++;
		}
	public void water_lcx() {
		for(int i=0;i<last;i++) {
			treeSet[i].water_lcx();
		}
	}
}

