package persons;

import lands.land_mzc;
import trees.tree_mzc;

public class person_mzc {
    String name = "mzc";
    land_mzc land=new land_mzc();
    public String toString() {
        System.out.print(name+"    "+land.landID+"    ");
        for (int i = 0;; i++){
            if(land.treeSet[i]==null)
                break;
            System.out.print(land.treeSet[i].treeID+"    ");
            System.out.print(land.treeSet[i].water + "    ");
        }
        System.out.println();
        return name;
    }

    public void plant(){
        tree_mzc tree = new tree_mzc();
        land.plant(tree);
    }
    public void water() {
    	tree_mzc water = new tree_mzc();
    	land.water(water);

    }
    
//    public static void main(String args[]){
//        person_mzc mzc = new person_mzc();
//        mzc.plant();
//        mzc.water();
//        mzc.toString();
////        person_zgh zgh=new person_zgh();
////        zgh.plant();
////        zgh.toString();
////        person_mpz mpz=new person_mpz();
////        mpz.plant();
////        mpz.toString();
////        person_lcx lcx=new person_lcx();
////        lcx.plant();
////        lcx.toString();
//    }
}
