package lands;
import trees.tree_zgh;
public class land_zgh {
	public String landID;
	public tree_zgh treeSet[];
	public static int last;
	public land_zgh() {
		landID="zgh_land";
		treeSet=new tree_zgh[50];
		last=0;
	}
	
	public void plant(tree_zgh tree){
		treeSet[last]=tree;
		last++;
		}
	public void water() {
		for(int i=0;i<last;i++) {
			treeSet[i].water();
		}
	}
}

